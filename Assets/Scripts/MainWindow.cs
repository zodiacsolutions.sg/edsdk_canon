using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;
using CameraControl;

public class MainWindow :  IObserver
{
    private CameraController _controller = null;
    private ActionSource _actionSource = new ActionSource();
    private List<ActionListener> _actionListenerList = new List<ActionListener>();
    private IObserver formAs = null;
    //private Form _remoteCapture = null;
    private IntPtr[] _volumes = new IntPtr[2];
    private int _volume_count = 0;

    public MainWindow(ref CameraController controller)
    {
        
        uint err = EDSDKLib.EDSDK.EDS_ERR_OK;
        // Get initial volume
        EDSDKLib.EDSDK.EdsVolumeInfo outVolumeInfo;

        formAs = this;
        //InitializeComponent();

        _controller = controller;
        err = EDSDKLib.EDSDK.EdsGetChildCount(_controller.GetModel().Camera, out _volume_count);
        Debug.Log("_volume_count = "+_volume_count);
        if (_volume_count > 0)
        {
            err = EDSDKLib.EDSDK.EdsGetChildAtIndex(_controller.GetModel().Camera, 0, out _volumes[0]);
            err = EDSDKLib.EDSDK.EdsGetVolumeInfo(_volumes[0], out outVolumeInfo);
            if (outVolumeInfo.StorageType != (uint)EDSDKLib.EDSDK.EdsStorageType.Non)
            {
                //this.button2.Enabled = true;
                //this.button2.Text = "Memory Card 1 (" + outVolumeInfo.szVolumeLabel + ")";
                //this.button7.Enabled = true;
                //this.button7.Text = "Memory Card 1 (" + outVolumeInfo.szVolumeLabel + ")";
                Debug.Log(outVolumeInfo.szVolumeLabel);
            }
            if (_volume_count > 1)
            {
                err = EDSDKLib.EDSDK.EdsGetChildAtIndex(_controller.GetModel().Camera, 1, out _volumes[1]);
                err = EDSDKLib.EDSDK.EdsGetVolumeInfo(_volumes[1], out outVolumeInfo);
                if (outVolumeInfo.StorageType != (uint)EDSDKLib.EDSDK.EdsStorageType.Non)
                {
                    //this.button5.Enabled = true;
                    //this.button5.Text = "Memory Card 2 (" + outVolumeInfo.szVolumeLabel + ")";
                    //this.button8.Enabled = true;
                    //this.button8.Text = "Memory Card 2 (" + outVolumeInfo.szVolumeLabel + ")";
                    Debug.Log(outVolumeInfo.szVolumeLabel);
                }
            }
        }



        _actionListenerList.Add((ActionListener)_controller);

        _actionListenerList.ForEach(actionListener => _actionSource.AddActionListener(ref actionListener));


    }
    public void Update(Observable from, CameraEvent e)
    {
        CameraEvent.Type eventType = e.GetEventType();

        switch (eventType)
        {
            case CameraEvent.Type.SHUT_DOWN:

                Debug.Log("Camera is disconnected");

                

                break;

            case CameraEvent.Type.PROPERTY_CHANGED:
                Debug.Log("PROPERTY_CHANGED");
                break;

            case CameraEvent.Type.ANGLEINFO:
                Debug.Log("ANGLEINFO");
                break;

            case CameraEvent.Type.MOUSE_CURSOR:
                Debug.Log("MOUSE_CURSOR");
                break;

            default:
                break;
        }
    }

}
