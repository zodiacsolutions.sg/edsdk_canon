using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EDSDKLib;
using CameraControl;
using System.Runtime.InteropServices;
public class test : MonoBehaviour
{
    public CameraModel CM;
    public CameraController CC;

    public void Start()
    {
        Debug.Log("start");

        var result = EDSDK.EdsInitializeSDK();

        //CameraHandler = new SDKHandler();
        //print("EDSDK Initialization successful");
        Debug.Log($"{result}");

        CM = new CameraModel((System.IntPtr)0);
        CC = new CameraController(ref CM);
    }
    public void ConnectCam()
    {
        CC.Run();
        GetModel();
        //CameraControl.CameraController
    }
    public void Photo()
    {
        CC.TakePicture();
    }


    public void GetModel()
    {
        CM = CC.GetModel();
        Debug.Log($"{CM}");
    }

    private void OnDisable()
    {
        EDSDK.EdsTerminateSDK();
    }

    private void OnDestroy()
    {
        EDSDK.EdsTerminateSDK();
    }
   
}
