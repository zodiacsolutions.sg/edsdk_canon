using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace CameraControl
{
    public class CameraControlScript : MonoBehaviour
    {
        [SerializeField] bool isSDKLoaded = false;
        public void Start()
        {
            Debug.Log("start");

            // Initialization of SDK
            uint err = EDSDKLib.EDSDK.EdsInitializeSDK();


            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                isSDKLoaded = true;
                Debug.Log($"{err}");
            }

            // ... (Continue modifying the code as needed)
            IntPtr cameraList = IntPtr.Zero;
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                err = EDSDKLib.EDSDK.EdsGetCameraList(out cameraList);
            }

            //Acquisition of number of Cameras
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                int count = 0;
                err = EDSDKLib.EDSDK.EdsGetChildCount(cameraList, out count);
                if (count == 0)
                {
                    err = EDSDKLib.EDSDK.EDS_ERR_DEVICE_NOT_FOUND;
                }
            }

            //Acquisition of camera at the head of the list
            IntPtr camera = IntPtr.Zero;
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                err = EDSDKLib.EDSDK.EdsGetChildAtIndex(cameraList, 0, out camera);
            }

            //Acquisition of camera information
            EDSDKLib.EDSDK.EdsDeviceInfo deviceInfo;
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                err = EDSDKLib.EDSDK.EdsGetDeviceInfo(camera, out deviceInfo);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK && camera == IntPtr.Zero)
                {
                    err = EDSDKLib.EDSDK.EDS_ERR_DEVICE_NOT_FOUND;
                }
            }


            //Release camera list
            if (cameraList != IntPtr.Zero)
            {
                EDSDKLib.EDSDK.EdsRelease(cameraList);
            }

            //Create Camera model
            CameraModel model = null;
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                model = new CameraModel(camera);
            }

            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                Debug.Log("Cannot detect camera");
            }


            CameraController controller;
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                //Create CameraController
                controller = new CameraController(ref model);
                GCHandle handle = GCHandle.Alloc(controller);
                IntPtr ptr = GCHandle.ToIntPtr(handle);

                controller.Run();
            }

            // Termination of SDK
            if (isSDKLoaded)
            {
                EDSDKLib.EDSDK.EdsTerminateSDK();
            }
        }

           
    }
}
